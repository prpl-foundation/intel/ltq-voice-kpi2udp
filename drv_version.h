#ifndef _DRV_VERSION_H
#define _DRV_VERSION_H

/******************************************************************************

  Copyright 2008-2009 Infineon Technologies AG
  Copyright 2009-2014 Lantiq Deutschland GmbH
  Copyright 2017, Intel Corporation.

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/


#define MAJORSTEP    3
#define MINORSTEP    1
#define VERSIONSTEP  0
#define VERS_TYPE    0

#endif /* _DRV_VERSION_H */

